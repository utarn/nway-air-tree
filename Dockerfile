FROM mcr.microsoft.com/dotnet/runtime:7.0 AS base
WORKDIR /app
VOLUME /app/result

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build
WORKDIR /
COPY . .
RUN dotnet restore "ExternalOMT-Core/ExternalOMT-Core.csproj"
WORKDIR "/ExternalOMT-Core"
RUN dotnet build "ExternalOMT-Core.csproj" -c Release -o /app/build

FROM build AS publish
WORKDIR "/ExternalOMT-Core"
RUN dotnet publish "ExternalOMT-Core.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
COPY --from=build /script.sh .
RUN chmod +x /app/script.sh
CMD /app/script.sh
