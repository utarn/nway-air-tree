#!/bin/bash

cd /app
for i in {1000000..1000000000..1000000} # No of data
 do 
    for j in {1..9..1}		# Repeat for 10 times
    do
	for k in {20..120..20}  # Threshold for memory usage
	do
	    dotnet ExternalOMT-Core.dll $i /root/index/ 40 $k | tee -a /app/result/data.txt
	done
    done
 done

