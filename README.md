# External N-way Attribute-Based IR-Tree algorithm #

This is a part of algorithm described in the paper: 
A Scalable Approach for Index in Generic Location-aware Rank Query 
by Utharn Buranasaksee

## Usage ##
```
# docker pull utarn/nway-air-tree
# docker run -it -v /root/result:/app/result utarn/nway-air-tree

```

You will find the result in /root/result directory.