﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ExternalOMT_Core
{
    public class RectangleGenerator
    {
        public static void GenerateRectangleDataFile(int noRectangle = 100000000, string defaultpath = "d:\\data.bin")
        {
            using (FileStream fs = new FileStream(defaultpath, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    Random rand = new Random();

                    for (int i = 0; i <= (noRectangle * 2) - 1; i++)
                    {
                        bw.Write(Convert.ToSingle(rand.NextDouble() * 10));
                    }
                }
            }
        }

        public static void SaveRectangleDataFile(Rectangle[] rects, int noRectangle = 100000000, string defaultpath = "d:\\data.bin")
        {
            using (FileStream fs = new FileStream(defaultpath, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    Random rand = new Random();

                    for (int i = 0; i <= (noRectangle * 2) - 1; i++)
                    {
                        bw.Write(Convert.ToSingle(rand.NextDouble() * 10));
                    }
                }
            }
        }
    }
}
