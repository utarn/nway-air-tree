﻿using System.Collections.Generic;

namespace ExternalOMT_Core
{

    public class XOrdinateComparer : IComparer<Rectangle>
    {

        public int Compare(Rectangle x, Rectangle y)
        {
            return (int)(x.min[0] - y.min[0]);
        }
    }
    public class YOrdinateComparer : IComparer<Rectangle>
    {

        public int Compare(Rectangle x, Rectangle y)
        {
            return (int)(x.min[1] - y.min[1]);
        }
    }
 
    public class XNodeComparer : IComparer<Node>
    {
        public int Compare(Node x, Node y)
        {
            dynamic centerX = (x.MBR.min[0] + x.MBR.max[0]) / 2;
            dynamic centerY = (y.MBR.min[0] + y.MBR.max[0]) / 2;

            return (centerX - centerY);
        }
    }
    public class YNodeComparer : IComparer<Node>
    {
        public int Compare(Node x, Node y)
        {
            dynamic centerX = (x.MBR.min[1] + x.MBR.max[1]) / 2;
            dynamic centerY = (y.MBR.min[1] + y.MBR.max[1]) / 2;

            return (centerX - centerY);
        }
    }


}