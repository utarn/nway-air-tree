﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ExternalOMT_Core
{

    public class Node
    {
        public int NodeID { get; set; }
        public const int DIMENSION = 2;
        public Rectangle MBR { get; set; }
        public Rectangle[] Entries { get; set; }
        public Node[] ChildNodes { get; set; }
        public int?[] ChildNodesID { get; set; }

        public int Level { get; set; }
        public int Count { get; set; }


        public bool IsLeaf
        {
            get { return Level == 1; }
        }

        public bool IsEmpty
        {
            get { return Count == 0; }
        }

        public bool HasRoom
        {
            get { return !(Count == RTree.DEFAULT_MAX_NODE_ENTRIES); }
        }

        public Node() : this(RTree.DEFAULT_MAX_NODE_ENTRIES)
        {
        }

        public Node(int NodeID, bool ToLoaded, int capacity = 300)
        {
            this.NodeID = NodeID;

            if (ToLoaded)
            {
                Load(capacity);
            }
        }

        public Node(int capacity)
        {
            NodeID = NodeIdentifier.getNodeID();
            Level = 1;
            Entries = new Rectangle[capacity];
            ChildNodes = new Node[capacity];
            ChildNodesID = new int?[capacity];
            MBR = new Rectangle();
        }
        
        public void Save()
        {
            var filename = $"{Program.basePath}{Path.DirectorySeparatorChar}{NodeIdentifier.basePath}{Path.DirectorySeparatorChar}{NodeID}.index";
            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    bw.Write(MBR.min[0]);
                    bw.Write(MBR.min[1]);
                    bw.Write(MBR.max[0]);
                    bw.Write(MBR.max[1]);
                    bw.Write(Level);
                    bw.Write(Count);
                    for (int i = 0; i < Count; i++)
                    {
                        if (IsLeaf)
                        {
                            bw.Write(Entries[i].min[0]);
                            bw.Write(Entries[i].min[1]);
                            bw.Write(Entries[i].max[0]);
                            bw.Write(Entries[i].max[1]);
                        }
                        else
                        {
                            bw.Write(ChildNodesID[i].Value);
                        }
                    }
                }
            }

        }

        public void Load(int capacity = 300)
        {
            Entries = new Rectangle[capacity];
            ChildNodes = new Node[capacity];
            ChildNodesID = new int?[capacity];
            MBR = new Rectangle();
            var filename = $"{Program.basePath}{Path.DirectorySeparatorChar}{NodeIdentifier.basePath}{Path.DirectorySeparatorChar}{NodeID}.index";

            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {
                    MBR.min[0] = br.ReadSingle();
                    MBR.min[1] = br.ReadSingle();
                    MBR.max[0] = br.ReadSingle();
                    MBR.max[1] = br.ReadSingle();
                    Level = br.ReadInt32();
                    Count = br.ReadInt32();
                    for (int i = 0; i < Count; i++)
                    {
                        if (IsLeaf)
                        {
                            Entries[i] = new Rectangle();
                            Entries[i].min[0] = br.ReadSingle();
                            Entries[i].min[1] = br.ReadSingle();
                            Entries[i].max[0] = br.ReadSingle();
                            Entries[i].max[1] = br.ReadSingle();
                        }
                        else
                        {
                            ChildNodesID[i] = br.ReadInt32();
                        }
                    }
                }
            }
        }

        public void LoadChild(int index)
        {
            var child = new Node(ChildNodesID[index].Value, true);
            ChildNodes[index] = child;
        }

        public void Dispose()
        {
            Entries = null;
            ChildNodes = null;
            ChildNodesID = null;
            //MBR = null;
        }

        public void RecalculateMBR()
        {
            for (int i = 0; i <= Count - 1; i++)
            {
                for (int j = 0; j <= DIMENSION - 1; j++)
                {
                    if (i == 0)
                    {
                        MBR.min[j] = float.MaxValue;
                        MBR.max[j] = float.MinValue;
                    }
                    if (this.IsLeaf)
                    {
                        if (Entries[i].min[j] < MBR.min[j])
                        {
                            MBR.min[j] = Entries[i].min[j];
                        }
                        if (Entries[i].max[j] > MBR.max[j])
                        {
                            MBR.max[j] = Entries[i].max[j];
                        }
                    }
                    else
                    {
                        if (ChildNodes[i].MBR.min[j] < MBR.min[j])
                        {
                            MBR.min[j] = ChildNodes[i].MBR.min[j];
                        }
                        if (ChildNodes[i].MBR.max[j] > MBR.max[j])
                        {
                            MBR.max[j] = ChildNodes[i].MBR.max[j];
                        }
                    }

                }
            }
        }
        public void AddEntry(Rectangle r)
        {
            this.Entries[Count] = r;
            Count += 1;
            //Me.RecalculateMBR()
        }
        public void AddChild(Node n)
        {
            this.ChildNodes[Count] = n;
            this.ChildNodesID[Count] = n.NodeID;
            Count += 1;
            //Me.RecalculateMBR()
        }
        public void RemoveEntry(int index)
        {
            if (this.IsLeaf)
            {
                this.Entries[index] = this.Entries[Count - 1];
                this.Entries[Count - 1] = null;
                Count -= 1;
                //Me.RecalculateMBR()
            }
            else
            {
                this.ChildNodes[index] = this.ChildNodes[Count - 1];
                this.ChildNodesID[index] = this.ChildNodesID[Count - 1];
                this.ChildNodes[Count - 1] = null;
                this.ChildNodesID[Count - 1] = null;
                Count -= 1;
                //Me.RecalculateMBR()
            }

        }
        public override string ToString()
        {
            float x1 = MBR.min[0];
            float y1 = MBR.min[1];
            float x2 = MBR.max[0];
            float y2 = MBR.max[1];

            return $"({x1},{y1}), ({x2},{y2}) Area = {MBR.Area()}";
        }
    }



}
