while sleep 1; do 
free -m | awk -v date="$(date +"%D %T")" '
BEGIN { 
	ORS=","
	print date
} 


NR==2 {
	printf "MemoryUsage,%s",$3
}  

NR==3{
	printf ",%s\n",$3
}
';

done;


