﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExternalOMT_Core
{
    public class Rectangle
    {

        public const int DIMENSIONS = 2;
        public float[] min;
        public float[] max;

        public Rectangle()
        {
            min = new float[DIMENSIONS];
            max = new float[DIMENSIONS];
        }

        public Rectangle(float x1, float y1, float x2, float y2)
        {
            min = new float[DIMENSIONS];
            max = new float[DIMENSIONS];
            SetValue(x1, y1, x2, y2);
        }

        public Rectangle(float[] min, float[] max)
        {
            if (min.Length != DIMENSIONS || max.Length != DIMENSIONS)
            {
                throw new Exception("Error in Rectangle constructor: " + "min and max arrays must be of length " + DIMENSIONS);
            }

            this.min = new float[DIMENSIONS];
            this.max = new float[DIMENSIONS];
            SetValue(min, max);
        }

        public void SetValue(float x1, float y1, float x2, float y2)
        {
            min[0] = Math.Min(x1, x2);
            min[1] = Math.Min(y1, y2);
            max[0] = Math.Max(x1, x2);
            max[1] = Math.Max(y1, y2);
        }

        //*
        public void SetValue(float[] min, float[] max)
        {
            Array.Copy(min, 0, this.min, 0, DIMENSIONS);
            Array.Copy(max, 0, this.max, 0, DIMENSIONS);
        }

        public Rectangle Copy()
        {
            return new Rectangle(min, max);
        }

        public float Enlargement(Rectangle r)
        {
            float enlargedArea = (Math.Max(max[0], r.max[0]) - Math.Min(min[0], r.min[0])) * (Math.Max(max[1], r.max[1]) - Math.Min(min[1], r.min[1]));
            return enlargedArea - Area();
        }

        public float Area()
        {
            return (max[0] - min[0]) * (max[1] - min[1]);
        }


        public void Merge(Rectangle r)
        {
            for (int i = 0; i <= DIMENSIONS - 1; i++)
            {
                if (r.min[i] < min[i])
                {
                    min[i] = r.min[i];
                }
                if (r.max[i] > max[i])
                {
                    max[i] = r.max[i];
                }
            }
        }

        //*
        //         * Find the the union of this rectangle and the passed rectangle.
        //         * Neither rectangle is altered
        //         * 
        //         * @param r The rectangle to union with this rectangle
        //         

        public Rectangle Union(Rectangle r)
        {
            Rectangle functionReturnValue = default(Rectangle);
            //Dim union As Rectangle = Me.copy()
            functionReturnValue = this.Copy();
            functionReturnValue.Merge(r);
            return functionReturnValue;
        }

        public bool CompareArrays(float[] a1, float[] a2)
        {
            if ((a1 == null) || (a2 == null))
            {
                return false;
            }
            if (a1.Length != a2.Length)
            {
                return false;
            }

            for (int i = 0; i <= a1.Length - 1; i++)
            {
                if (a1[i] != a2[i])
                {
                    return false;
                }
            }
            return true;
        }

        public bool SameObject(object o)
        {
            return base.Equals(o);
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            // min coordinates
            sb.Append("(");
            for (int i = 0; i <= DIMENSIONS - 1; i++)
            {
                if (i > 0)
                {
                    sb.Append(", ");
                }
                sb.Append(min[i]);
            }
            sb.Append("), (");

            // max coordinates
            for (int i = 0; i <= DIMENSIONS - 1; i++)
            {
                if (i > 0)
                {
                    sb.Append(", ");
                }
                sb.Append(max[i]);
            }
            sb.Append(") ");

            return sb.ToString();

        }

        public static byte[] ToBytes(Rectangle[] rect)
        {
            byte[] output = new byte[rect.Length * 8];
            float[] floatArrays = new float[rect.Length * 2];
            for (int i = 0; i < rect.Length * 2; i += 2)
            {
                floatArrays[i] = rect[i / 2].min[0];
                floatArrays[i + 1] = rect[i / 2].min[1];
            }
            Buffer.BlockCopy(floatArrays, 0, output, 0, output.Length);
            return output;
        }

        public static Rectangle[] FromBytes(byte[] buffer)
        {
            float[] temp = new float[buffer.Length / 4];
            Buffer.BlockCopy(buffer, 0, temp, 0, buffer.Length);
            Rectangle[] output = new Rectangle[buffer.Length / 8];
            for (int i = 0; i < output.Length; i++)
            {
                output[i] = new Rectangle(temp[i * 2], temp[i * 2 + 1], temp[i * 2], temp[i * 2 + 1]);
            }
            return output;
        }
    }
}
