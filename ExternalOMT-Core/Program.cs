﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace ExternalOMT_Core
{
    class Program
    {

        public static string basePath = "d:";

        static void Main(string[] args)
        {
            //Console.WriteLine(args.Length);
            // foreach (var arg in args)
            // {
                // Console.WriteLine(arg);
            // }
            if (args.Length == 0)
            {
                Console.WriteLine("parameter list : <noOfObjects> <basePath> <fanout>");
                return;
            }
            int noOfData = 100000;
            if (!string.IsNullOrEmpty(args[0])) 
                noOfData = int.Parse(args[0]);


            if (!string.IsNullOrEmpty(args[1]))
                basePath = args[1];

            if (!string.IsNullOrEmpty(args[2]))
                RTree.DEFAULT_MAX_NODE_ENTRIES = int.Parse(args[2]);

            if (!string.IsNullOrEmpty(args[3]))
                ExternalInterface.TARGET_MEMORY = int.Parse(args[3]);

            if (!Directory.Exists($"{basePath}{Path.DirectorySeparatorChar}raw"))
            {
                Directory.CreateDirectory($"{basePath}{Path.DirectorySeparatorChar}raw");
            }

            // Performing optimized disk based benchmarking
            string defaultPath = $@"{basePath}{Path.DirectorySeparatorChar}raw{Path.DirectorySeparatorChar}data0.bin";
            RectangleGenerator.GenerateRectangleDataFile(noOfData, defaultPath);
            NodeIdentifier.basePath = "extInterface";
            var targetpath = $"{basePath}{Path.DirectorySeparatorChar}{NodeIdentifier.basePath}";
            if (!Directory.Exists(targetpath))
            {
                Directory.CreateDirectory(targetpath);
            }
            else
            {
                Directory.Delete(targetpath, true);
                Directory.CreateDirectory(targetpath);
            }
            var rawSize = new FileInfo(defaultPath).Length;
            Stopwatch watch = new Stopwatch();
            // need to debug sorting if it 's working the same on the memory
            XOrdinateComparer xComparer1 = new XOrdinateComparer();
            YOrdinateComparer yComparer1 = new YOrdinateComparer();
            RTree.SetComparer(xComparer1, yComparer1);

            watch.Start();
            var startDate1 = DateTime.Now;
            RTree tree = RTree.Packing(defaultPath, noOfData);
            var stopDate1 = DateTime.Now;
            watch.Stop();
            var elapsedTime1 = stopDate1 - startDate1;
            long indexSize = Directory.GetFiles(targetpath, "*", SearchOption.AllDirectories).Sum(t => new FileInfo(t).Length);

            // Adding predicate for attributed-IR-Tree to estimate index size
            for (long predicate = 0; predicate <= 5; predicate++)
            {
                var newRawSize = rawSize + (rawSize * predicate / (long)2);
                var newIndexSize = indexSize + ((long)NodeIdentifier.ID * (long)4 * (long)predicate) + ((long)noOfData * (long)4 * (long)predicate);

                var rawMB = newRawSize / 1048576.0;
                var indexMB = newIndexSize / 1048576.0;
                var allIndexMB = indexMB + (indexMB * (predicate - 1));
                var output1 = $"{elapsedTime1.TotalSeconds},disk,{noOfData},{RTree.DEFAULT_MAX_NODE_ENTRIES}," +
                              $"{predicate},{NodeIdentifier.ID},{indexMB:00.000},{allIndexMB:00.000}";
                Console.WriteLine(output1);
            }

            // Performing unoptmized memory based benchmarking
            NodeIdentifier.basePath = "memory";
            NodeIdentifier.reset();
            targetpath = $"{basePath}{Path.DirectorySeparatorChar}{NodeIdentifier.basePath}";
            if (!Directory.Exists(targetpath))
            {
                Directory.CreateDirectory(targetpath);
            }
            else
            {
                Directory.Delete(targetpath, true);
                Directory.CreateDirectory(targetpath);
            }
            
            watch.Reset();
            watch.Start();
            var startDate2 = DateTime.Now;
            defaultPath = $@"{basePath}{Path.DirectorySeparatorChar}data0.bin";
            ExternalInterface data = new ExternalInterface(defaultPath);
            data.Open();
            Rectangle[] rect2 = data.GetRectangleByBlock(0, noOfData);
            var bytes = Rectangle.ToBytes(rect2);
            var rects = Rectangle.FromBytes(bytes);
            data.Close();
            RTree tree2 = RTree.Packing(ref rect2);
            var stopDate2 = DateTime.Now;
            watch.Stop();
            var elapsedTime2 = stopDate2 - startDate2;
            var output2 =
                $"{elapsedTime2.TotalSeconds},memory,{noOfData},{RTree.DEFAULT_MAX_NODE_ENTRIES}," +
                $"0,{NodeIdentifier.ID},{indexSize:00.000}";
            Console.WriteLine(output2);

        }
    }
}
