﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ExternalOMT_Core
{
    public class RTree
    {
        public static int DEFAULT_MAX_NODE_ENTRIES { get; set; } = 100;
        public static int DEFAULT_MIN_NODE_ENTRIES
        {
            get
            {
                return DEFAULT_MAX_NODE_ENTRIES / 2;
            }
        }

        public Node root;

        public int Height
        {
            get { return root.Level; }
        }

        public static void SetComparer(IComparer<Rectangle> xx, IComparer<Rectangle> yy)
        {
            xcomparer = xx;
            ycomparer = yy;
        }

        public static RTree LoadFromDisk()
        {
            RTree result = new RTree();
            result.root = new Node();
            result.root.NodeID = 0;
            result.root.Load(RTree.DEFAULT_MAX_NODE_ENTRIES);
            return result;
        }

        public static RTree Packing(ref Rectangle[] allRects)
        {
            if (xcomparer == null || ycomparer == null)
            {
                return null;
            }
            RTree result = new RTree();
            Node rootNode = PackOMT(ref allRects, 0, allRects.Length - 1, 0);
            result.root = rootNode;
            return result;
        }

        static IComparer<Rectangle> xcomparer;

        static IComparer<Rectangle> ycomparer;
        public static RTree Packing(string externalRect, int noOfRectangle)
        {
            if (xcomparer == null || ycomparer == null)
            {
                return null;
            }
            RTree result = new RTree();
            ExternalInterface externalRectInterface = new ExternalInterface(externalRect);
            externalRectInterface.Open();
            Node rootNode = PackOMT(externalRectInterface, 0, noOfRectangle - 1, 0);
            externalRectInterface.Close();
            result.root = rootNode;
            return result;
        }

        private static Node PackOMT(ExternalInterface extInterface, int left, int right, int height)
        {
            int N = right - left + 1;
            int M = DEFAULT_MAX_NODE_ENTRIES;
            Node returnNode;

            if (N <= M)
            {
                //Console.WriteLine("Packing Level 1");
                Node newNode = new Node();
                newNode.Level = 1;

                Rectangle[] toAdd = extInterface.GetRectangleByBlock(left, N);
                for (int i = 0; i < toAdd.Length; i++)
                {
                    newNode.AddEntry(toAdd[i]);
                }
                newNode.RecalculateMBR();
                newNode.Save();
                newNode.Dispose();
                NodeIdentifier.countLeafNode();
                return newNode;
            }
            //bool top = false;
            if (height == 0)
            {
                //top = true;
                // for root entry
                height = (int)Math.Ceiling((double)Math.Log(N, M));
                // height of the tree
                M = (int)Math.Ceiling(N / Math.Pow(M, height - 1));
                // no of entries in root node
            }

            returnNode = new Node();
            returnNode.Level = height;

            int N2 = (int)Math.Ceiling((decimal)N / M);
            int N1 = (int)(N2 * Math.Ceiling(Math.Sqrt(M)));

            int right2 = 0;
            int right3 = 0;

            //extInterface.Sort(left, right - left, xcomparer);
            extInterface.Sort(left, right - 1, xcomparer);

            for (int i = left; i <= right; i += N1)
            {
                right2 = Math.Min(i + N1 - 1, right);
                extInterface.Sort(i, right2 - 1, ycomparer);
                for (int j = i; j <= right2; j += N2)
                {
                    right3 = Math.Min(j + N2 - 1, right2);
                    Node childValue = PackOMT(extInterface, j, right3, height - 1);
                    returnNode.AddChild(childValue);
                }
            }
            returnNode.RecalculateMBR();
            returnNode.Save();
            returnNode.Dispose();
            //Console.WriteLine($"Packing Level {returnNode.Level}");
            return returnNode;
            //return null;
        }
        private static Node PackOMT(ref Rectangle[] allRects, int left, int right, int height)
        {

            int N = right - left + 1;
            int M = DEFAULT_MAX_NODE_ENTRIES;
            Node returnNode;
            if (N <= M)
            {
                //Console.WriteLine("Packing Level 1");
                Node newNode = new Node();
                newNode.Level = 1;
                for (int i = left; i <= right; i++)
                {
                    newNode.AddEntry(allRects[i]);
                }
                newNode.RecalculateMBR();
                newNode.Save();
                newNode.Dispose();
                return newNode;
            }

            //bool top = false;
            if (height == 0)
            {
                //top = true;
                // for root entry
                height = (int)Math.Ceiling(Math.Log(N, M));
                // height of the tree
                M = (int)Math.Ceiling(N / Math.Pow(M, height - 1));
                // no of entries in root node
            }
            returnNode = new Node();
            returnNode.Level = height;

            int N2 = (int)Math.Ceiling((decimal)N / M);
            int N1 = (int)(N2 * Math.Ceiling(Math.Sqrt(M)));

            int right2 = 0;
            int right3 = 0;
            Array.Sort(allRects, left, right - left + 1, xcomparer);
            for (int i = left; i <= right; i += N1)
            {
                right2 = Math.Min(i + N1 - 1, right);
                Array.Sort(allRects, i, right2 - i, ycomparer);
                for (int j = i; j <= right2; j += N2)
                {
                    right3 = Math.Min(j + N2 - 1, right2);
                    Node childValue = PackOMT(ref allRects, j, right3, height - 1);
                    int test = returnNode.Count;
                    returnNode.AddChild(childValue);
                }
            }

            returnNode.RecalculateMBR();
            returnNode.Save();
            returnNode.Dispose();
            //Console.WriteLine($"Packing Level {returnNode.Level}");
            return returnNode;

        }


        public void Insert(ref Rectangle newRect)
        {
            Node insertingNode = root;
            Stack<Node> parentTrace = new Stack<Node>();
            while (!insertingNode.IsLeaf)
            {
                int nextBranch = ChooseLeaf(ref insertingNode, ref newRect);
                parentTrace.Push(insertingNode);
                insertingNode = insertingNode.ChildNodes[nextBranch];
            }

            Node SplitedNode = default(Node);
            if (insertingNode.HasRoom)
            {
                insertingNode.AddEntry(newRect);
            }
            else
            {
                SplitedNode = SplitNode(ref insertingNode, ref newRect);
            }

            Node newNode = AdjustTree(ref insertingNode, ref SplitedNode, parentTrace);

            if (newNode != null)
            {
                Node oldRoot = root;
                Node newRoot = new Node();
                newRoot.Level = root.Level + 1;
                newRoot.AddChild(oldRoot);
                newRoot.AddChild(newNode);
                this.root = newRoot;
            }
        }
        private Node SplitNode(ref Node oldNode, ref Rectangle newRect)
        {
            // Array of rectangle distribution
            int[] distribution = new int[oldNode.Count + 1];
            for (int i = 0; i <= distribution.Count() - 1; i++)
            {
                distribution[i] = -1;
            }

            // Array of pointer to all overflowing rectangle
            Rectangle[] data = new Rectangle[oldNode.Count + 1];
            for (int index = 0; index <= oldNode.Count - 1; index++)
            {
                if (oldNode.IsLeaf)
                {
                    data[index] = oldNode.Entries[index];
                }
                else
                {
                    data[index] = oldNode.ChildNodes[index].MBR;
                }
            }
            data[oldNode.Count] = newRect;

            // Seeds
            int seed0 = 0;
            int seed1 = 0;
            PickSeeds(ref data, ref seed0, ref seed1);
            distribution[seed0] = 0;
            distribution[seed1] = 1;

            int NoOfStay = 1;
            int NoOfMove = 1;


            while (NoOfStay + NoOfMove <= DEFAULT_MAX_NODE_ENTRIES)
            {
                if (DEFAULT_MAX_NODE_ENTRIES - NoOfStay == DEFAULT_MIN_NODE_ENTRIES)
                {
                    for (int i = 0; i <= distribution.Count() - 1; i++)
                    {
                        if (distribution[i] == -1)
                        {
                            distribution[i] = 1;
                            NoOfMove += 1;
                        }
                    }
                    continue;
                }

                if (DEFAULT_MAX_NODE_ENTRIES - NoOfMove == DEFAULT_MIN_NODE_ENTRIES)
                {
                    for (int i = 0; i <= distribution.Count() - 1; i++)
                    {
                        if (distribution[i] == -1)
                        {
                            distribution[i] = 0;
                            NoOfStay += 1;
                        }
                    }
                    continue;
                }



                int StayOrMove = PickNext(ref data, ref distribution, seed0, seed1);
                if (StayOrMove == 0)
                {
                    NoOfStay += 1;
                }
                else
                {
                    NoOfMove += 1;
                }

            }

            // Physically move data to appropriate node
            Node newNode = new Node();
            newNode.Level = oldNode.Level;
            Rectangle tempData = default(Rectangle);
            for (int i = data.Count() - 1; i >= 0; i += -1)
            {
                if (i != data.Count() - 1)
                {
                    if (distribution[i] == 1)
                    {
                        newNode.AddEntry(oldNode.Entries[i]);
                        oldNode.RemoveEntry(i);
                    }
                }
                else
                {
                    if (distribution[i] == 0)
                    {
                        if (oldNode.HasRoom)
                        {
                            oldNode.AddEntry(data[i]);
                        }
                        else
                        {
                            tempData = data[i];
                        }
                    }
                    else
                    {
                        newNode.AddEntry(data[i]);
                    }

                }
            }

            if (tempData != null)
            {
                oldNode.AddEntry(tempData);
            }
            return newNode;
        }
        private Node SplitNode(ref Node oldNode, ref Node additionNode)
        {
            // Array of rectangle distribution
            int[] distribution = new int[oldNode.Count + 1];
            for (int i = 0; i <= distribution.Count() - 1; i++)
            {
                distribution[i] = -1;
            }

            // Array of pointer to all overflowing rectangle
            Node[] data = new Node[oldNode.Count + 1];
            for (int index = 0; index <= oldNode.Count - 1; index++)
            {
                data[index] = oldNode.ChildNodes[index];
            }
            data[oldNode.Count] = additionNode;


            Rectangle[] data2 = new Rectangle[oldNode.Count + 1];
            for (int index = 0; index <= oldNode.Count - 1; index++)
            {
                data2[index] = oldNode.ChildNodes[index].MBR;
            }
            data2[oldNode.Count] = additionNode.MBR;

            // Seeds
            int seed0 = 0;
            int seed1 = 0;
            PickSeeds(ref data2, ref seed0, ref seed1);
            distribution[seed0] = 0;
            distribution[seed1] = 1;

            int NoOfStay = 1;
            int NoOfMove = 1;

            while (NoOfStay + NoOfMove <= DEFAULT_MAX_NODE_ENTRIES)
            {
                if (DEFAULT_MAX_NODE_ENTRIES - NoOfMove == DEFAULT_MIN_NODE_ENTRIES)
                {
                    for (int i = 0; i <= distribution.Count() - 1; i++)
                    {
                        if (distribution[i] == -1)
                        {
                            distribution[i] = 0;
                            NoOfStay += 1;
                        }
                    }
                    continue;
                }

                if (DEFAULT_MAX_NODE_ENTRIES - NoOfStay == DEFAULT_MIN_NODE_ENTRIES)
                {
                    for (int i = 0; i <= distribution.Count() - 1; i++)
                    {
                        if (distribution[i] == -1)
                        {
                            distribution[i] = 1;
                            NoOfMove += 1;
                        }
                    }
                    continue;
                }


                int StayOrMove = PickNext(ref data2, ref distribution, seed0, seed1);
                if (StayOrMove == 0)
                {
                    NoOfStay += 1;
                }
                else
                {
                    NoOfMove += 1;
                }

            }

            // Physically move data to appropriate node
            Node tempData = default(Node);
            Node newNode = new Node();
            newNode.Level = oldNode.Level;

            for (int i = data.Count() - 1; i >= 0; i += -1)
            {
                if (i != data.Count() - 1)
                {
                    if (distribution[i] == 1)
                    {
                        newNode.AddChild(data[i]);
                        oldNode.RemoveEntry(i);
                    }
                }
                else
                {
                    if (distribution[i] == 0)
                    {
                        if (oldNode.HasRoom)
                        {
                            oldNode.AddChild(additionNode);
                        }
                        else
                        {
                            tempData = additionNode;
                        }

                    }
                    else
                    {
                        newNode.AddChild(additionNode);
                    }

                }
            }

            if (tempData != null)
            {
                oldNode.AddChild(tempData);
            }

            return newNode;

        }
        private void PickSeeds(ref Rectangle[] data, ref int seed0, ref int seed1)
        {
            float tempEnlargement = 0;
            float maxEnlargement = float.MinValue;

            for (int i = 0; i <= data.Count() - 2; i++)
            {
                for (int j = 1; j <= data.Count() - 1; j++)
                {
                    tempEnlargement = data[i].Enlargement(data[j]);
                    if (maxEnlargement < tempEnlargement)
                    {
                        maxEnlargement = tempEnlargement;
                        seed0 = i;
                        seed1 = j;
                    }
                }
            }

        }
        private int ChooseLeaf(ref Node node, ref Rectangle rect)
        {
            int Leastindex = 0;
            float LeastEnlarge = float.MaxValue;
            for (int index = 0; index <= node.Count - 1; index++)
            {
                float enlarge = 0;
                if (node.IsLeaf)
                {
                    enlarge = node.Entries[index].Enlargement(rect);
                }
                else
                {
                    enlarge = node.ChildNodes[index].MBR.Enlargement(rect);
                }
                if (enlarge < LeastEnlarge)
                {
                    LeastEnlarge = enlarge;
                    Leastindex = index;
                }
            }
            return Leastindex;
        }
        private int PickNext(ref Rectangle[] data, ref int[] distribution, int seed0, int seed1)
        {
            float maxDifference = 0;
            int nextIndex = 0;
            for (int i = 0; i <= data.Count() - 1; i++)
            {
                if (distribution[i] == -1)
                {
                    float area0 = data[seed0].Enlargement(data[i]);
                    float area1 = data[seed1].Enlargement(data[i]);
                    float differentArea = area0 - area1;

                    if (Math.Abs(differentArea) > Math.Abs(maxDifference))
                    {
                        maxDifference = differentArea;
                        nextIndex = i;
                    }

                }
            }

            if (maxDifference < 0)
            {
                distribution[nextIndex] = 0;
                return 0;
            }
            else
            {
                distribution[nextIndex] = 1;
                return 1;
            }

        }
        private Node AdjustTree(ref Node insertingNode, ref Node SplitedNode, Stack<Node> parentTrace)
        {

            while (insertingNode.Level != Height)
            {
                Node Parent = parentTrace.Pop();

                Parent.MBR.Merge(insertingNode.MBR);

                Node newNode = default(Node);
                if (SplitedNode != null)
                {
                    if (Parent.HasRoom)
                    {
                        Parent.AddChild(SplitedNode);
                    }
                    else
                    {
                        newNode = SplitNode(ref Parent, ref SplitedNode);
                    }
                }

                insertingNode = Parent;
                SplitedNode = newNode;

                Parent = null;
                newNode = null;
            }

            return SplitedNode;

        }


    }


}
