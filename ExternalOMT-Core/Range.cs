﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExternalOMT_Core
{
    public class Range
    {

        public float Min { get; set; }
        public float Max { get; set; }
        public bool IsClosedMin { get; set; }
        public bool IsClosedMax { get; set; }

        public Range()
        {
        }

        public Range(string value)
        {
            if (value.Contains("-"))
            {
                float value1 = float.Parse(value.Split('-')[0]);
                float value2 = float.Parse(value.Split('-')[1]);
                SetValue(value1, value2);
            }
            else
            {
                SetValue(float.Parse(value), float.Parse(value));
            }
        }

        public Range(float min, float max)
        {
            SetValue(min, max);
        }

        public Range(float min, float max, bool closedMin, bool closedMax)
        {
            SetValue(min, max);
            IsClosedMin = closedMin;
            IsClosedMax = closedMax;
        }

        private void SetValue(float min, float max)
        {
            this.Min = Math.Min(min, max);
            this.Max = Math.Max(min, max);

        }

        public bool IsInRange(float value)
        {
            if (IsClosedMin & IsClosedMax)
            {
                return (Min <= value && Max >= value ? true : false);
            }
            else if (!IsClosedMin & IsClosedMax)
            {
                return (Min < value && Max >= value ? true : false);
            }
            else if (IsClosedMin & !IsClosedMax)
            {
                return (Min <= value && Max > value ? true : false);
            }
            else if (!IsClosedMin & !IsClosedMax)
            {
                return (Min < value && Max > value ? true : false);
            }
            else
            {
                return false;
            }
        }
    }

}
