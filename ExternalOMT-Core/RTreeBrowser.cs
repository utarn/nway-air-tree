﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ExternalOMT_Core
{
    public class RTreeBrowser
    {
        private string _filename;
        private FileStream _fs;

        private BinaryReader _br;
        public RTreeBrowser(string filename)
        {
            _filename = filename;

        }

        public void PrintTree()
        {
            _fs = new FileStream(_filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            _br = new BinaryReader(_fs);
            Console.Clear();
            dynamic i = 0;
            while (_fs.Position < _fs.Length)
            {
                dynamic x = _br.ReadSingle();
                dynamic y = _br.ReadSingle();
                Rectangle r = new Rectangle(x, y, x, y);
                Console.WriteLine($"{i} : {r.ToString()}");
                i += 1;
            }
            _br.Dispose();
            _fs.Dispose();

        }


    }
}
