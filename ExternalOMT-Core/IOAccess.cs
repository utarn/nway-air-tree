﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExternalOMT_Core
{
    public class IOAccess
    {
        private int _diskAccess = 0;
        public int DiskAccess
        {
            get
            {
                return _diskAccess;
            }
        }

        private int _dataAccess = 0;
        public int DataAccess
        {
            get
            {
                return _dataAccess;
            }
        }

        public void ReadDisk()
        {
            _diskAccess++;
        }

        public void ReadData()
        {
            _dataAccess++;
        }
    }

    public static class NodeIdentifier
    {
        public static int ID { get; set; } = 0;

        public static int getNodeID()
        {
            ID++;
            return ID;
        }

        public static void reset()
        {
            ID = 0;
        }

        public static int LeafNode { get; set; } = 0;

        public static void countLeafNode()
        {
            LeafNode++;
        }
        public static string basePath = @"extInterface";
    }
}
