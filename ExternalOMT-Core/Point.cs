﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExternalOMT_Core
{
    public class Point
    {
        private const int DIMENSIONS = 2;
        /// <summary>
        /// The (x, y) coordinates of the point.
        /// </summary>

        internal float[] coordinates;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="x">The x coordinate of the point</param>
        /// <param name="y">The y coordinate of the point</param>
        public Point(float x, float y)
        {
            coordinates = new float[DIMENSIONS] {x, y };
        }

    }
}
