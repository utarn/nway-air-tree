﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ExternalOMT_Core
{
    public class ExternalInterface
    {

        public static int TARGET_MEMORY { get; set; } = 30;
        public static long MAX_MEMORY; 
        public static int NODE_SIZE = 8;
        //public const int DISKBLOCK_SIZE = 40960;
        public static int tempId = 0;
        private string _filename;
        private FileStream _fs;
        private BinaryReader _br;
        private BinaryWriter _bw;
        //private long _lastPosition;


        public ExternalInterface(string filename)
        {
            MAX_MEMORY = (TARGET_MEMORY * (1024 * 1024));
            _filename = filename;
        }

        public void Close()
        {
            _bw.Dispose();
            _br.Dispose();
            _fs.Dispose();
            GC.SuppressFinalize(_bw);
            GC.SuppressFinalize(_br);
            GC.SuppressFinalize(_fs);
        }

        public void Open()
        {
            _fs = new FileStream(_filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None, 81920 );
            _br = new BinaryReader(_fs);
            _bw = new BinaryWriter(_fs);
        }

        //public void PrintTree()
        //{
        //    Console.Clear();
        //    int i = 0;
        //    _fs.Seek(0, SeekOrigin.Begin);
        //    while (_fs.Position < _fs.Length)
        //    {
        //        float x = _br.ReadSingle();
        //        float y = _br.ReadSingle();
        //        Rectangle r = new Rectangle(x, y, x, y);
        //        Console.WriteLine($"{i} : {r.ToString()}");
        //        i++;
        //    }

        //    //_lastPosition = _fs.Position;
        //    Console.WriteLine("Wait for enter");
        //    Console.ReadLine();
        //}


        //public Rectangle GetRectangleByIndex(int index)
        //{
        //    try
        //    {
        //        long position = NODE_SIZE * index;
        //        _fs.Seek(position, SeekOrigin.Begin);
        //        float x = _br.ReadSingle();
        //        float y = _br.ReadSingle();
        //        Rectangle rect = new Rectangle(x, y, x, y);
        //        //_lastPosition = _fs.Position;
        //        return rect;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}

        public Rectangle[] GetRectangleByBlock(int index, int chunkSize)
        {
            Rectangle[] result;

            int totalBytes = chunkSize * ExternalInterface.NODE_SIZE;
            int position = ExternalInterface.NODE_SIZE * index;
            _fs.Seek(position, SeekOrigin.Begin);
            byte[] buffer = new byte[totalBytes];
            _br.Read(buffer, 0, totalBytes);
            result = Rectangle.FromBytes(buffer);
            return result;
        }

        //public void SetRectangleByIndex(int index, Rectangle rect)
        //{
        //    long position = NODE_SIZE * index;
        //    _fs.Seek(position, SeekOrigin.Begin);
        //    _bw.Write(rect.min[0]);
        //    _bw.Write(rect.min[1]);
        //}

        //public void SetRectangleByBlock(int index, List<Rectangle> rect)
        //{
        //    long position = NODE_SIZE * index;
        //    _fs.Seek(position, SeekOrigin.Begin);
        //    foreach (Rectangle r in rect)
        //    {
        //        _bw.Write(r.min[0]);
        //        _bw.Write(r.min[1]);
        //    }

        //}

        public void SetRectangleByBlock(int index, Rectangle[] rect)
        {
            long position = NODE_SIZE * index;
            _fs.Seek(position, SeekOrigin.Begin);
            byte[] buffer = Rectangle.ToBytes(rect);
            _bw.Write(buffer, 0, buffer.Length);
        }

        public void SetRectangleByBlock(int index, string rectTemp)
        {
            long position = NODE_SIZE * index;
            _fs.Seek(position, SeekOrigin.Begin);
            using (FileStream tempFs = new FileStream(rectTemp, FileMode.Open, FileAccess.Read, FileShare.None, 81920))
            {
                using (BinaryReader tempBr = new BinaryReader(tempFs))
                {

                    int toRead = (int)(tempFs.Length - tempFs.Position);
                    byte[] buffer = new byte[toRead];
                    tempBr.Read(buffer, 0, toRead);
                    _bw.Write(buffer, 0, toRead);                    
                }
            }
        }

        public void SaveListToFileAppend(string rectTemp, Rectangle[] rect)
        {
            using (FileStream tempFs = new FileStream(rectTemp, FileMode.Append, FileAccess.Write, FileShare.None, 81920))
            {
                using (BinaryWriter tempBw = new BinaryWriter(tempFs))
                {
                    byte[] buffer = Rectangle.ToBytes(rect);
                    tempBw.Write(buffer, 0, buffer.Length);
                }
            }
        }

        public void Sort(int left, int right, IComparer<Rectangle> comparer)
        {
            //Console.WriteLine($"Sorting {left} - {right}");
            int noChunks = CalculateNoOfChunks(left, right);
            bool firstLevel = true;
            float total = (right - left) + 1;
            int chunkSize = (int)Math.Ceiling(total / noChunks);
            int baseChunkSize = chunkSize;
            int baseToLoad = 1;
            int LtoLoad;
            int RtoLoad;
            while ((noChunks > 0))
            {
                if ((noChunks == 1))
                {
                    if (firstLevel)
                    {
                        Rectangle[] dataBlock = GetRectangleByBlock(left, right - left + 1);
                        Array.Sort(dataBlock, comparer);
                        SetRectangleByBlock(left, dataBlock);
                    }
                    else
                    {
                        noChunks /= 2;
                        continue;
                    }

                }

                LtoLoad = baseToLoad;
                RtoLoad = baseToLoad;
                string tempFile = $"{Program.basePath}{Path.DirectorySeparatorChar}{NodeIdentifier.basePath}{Path.DirectorySeparatorChar}temp-{tempId}.bin";
                tempId++;
                for (int i = 0; i < noChunks - 1; i += 2)
                {
                    int leftPtr = 0;
                    int rightPtr = 0;
                    Rectangle[] leftBlock = GetRectangleByBlock(left + leftPtr + i, baseChunkSize);
                    int limitRightBlock = Math.Min(baseChunkSize, (int)total - (chunkSize * (i + 1)));
                    // Change baseChunkSize ==> limitRightBlock :: Working!!
                    Rectangle[] rightBlock = GetRectangleByBlock(left + rightPtr + ((i + 1) * chunkSize), limitRightBlock);
                    LtoLoad--;
                    RtoLoad--;
                    // Changing List<> to []
                    //List<Rectangle> mergeBlock = new List<Rectangle>();
                    Rectangle[] mergeBlock = new Rectangle[baseChunkSize];
                    var mergeCount = 0;
                    int writeBackOffset = 0;
                    if (firstLevel)
                    {
                        Array.Sort(leftBlock, comparer);
                        Array.Sort(rightBlock, comparer);
                    }

                    while ((leftPtr < chunkSize) || (rightPtr < chunkSize))
                    {
                        Rectangle r1;
                        Rectangle r2;
                        if ((leftPtr == chunkSize))
                        {
                            int Rindex = rightPtr % baseChunkSize;
                            if (Rindex <= (rightBlock.Length - 1))
                            {
                                r2 = rightBlock[Rindex];
                                //mergeBlock.Add(r2);
                                mergeBlock[mergeCount++] = r2;
                            }

                            rightPtr++;
                        }
                        else if (rightPtr == Math.Min(chunkSize, rightBlock.Length))
                        {
                            int Lindex = leftPtr % baseChunkSize;
                            if (Lindex <= leftBlock.Length - 1)
                            {
                                r1 = leftBlock[Lindex];
                                //mergeBlock.Add(r1);
                                mergeBlock[mergeCount++] = r1;
                            }

                            leftPtr++;
                        }
                        else
                        {
                            r1 = leftBlock[(leftPtr % baseChunkSize)];
                            r2 = rightBlock[(rightPtr % baseChunkSize)];
                            if ((comparer.Compare(r1, r2) < 0))
                            {
                                mergeBlock[mergeCount++] = r1;
                                //mergeBlock.Add(r1);
                                leftPtr++;
                            }
                            else
                            {
                                //mergeBlock.Add(r2);
                                mergeBlock[mergeCount++] = r2;
                                rightPtr++;
                            }

                        }

                        //if (mergeBlock.Count >= baseChunkSize)
                        if (mergeCount >= baseChunkSize)
                        {
                            //  change to base chunk size of 4096 byte
                            if (firstLevel)
                            {
                                SetRectangleByBlock(left + writeBackOffset + (i * baseChunkSize), mergeBlock);
                                writeBackOffset = (writeBackOffset + baseChunkSize);
                                mergeBlock = new Rectangle[baseChunkSize];
                                mergeCount = 0;
                                //mergeBlock.Clear();
                            }
                            else
                            {
                                this.SaveListToFileAppend(tempFile, mergeBlock);
                                mergeBlock = new Rectangle[baseChunkSize];
                                mergeCount = 0;
                                //mergeBlock.Clear();
                            }

                        }

                        //  LOAD NEXT BLOCK
                        if (leftPtr == leftBlock.Length - 1 && LtoLoad > 0)
                        {
                            leftPtr++;
                            leftBlock = GetRectangleByBlock(left + leftPtr + i, baseChunkSize);
                            LtoLoad--;
                        }

                        if (rightPtr == rightBlock.Length - 1 && RtoLoad > 0)
                        {
                            rightPtr++;
                            rightBlock = GetRectangleByBlock(left + rightPtr + i, baseChunkSize);
                            RtoLoad--;
                        }

                    }

                    if (mergeCount >= baseChunkSize)
                    //if (mergeBlock.Count > 0)
                    {
                        if (firstLevel)
                        {
                            this.SetRectangleByBlock(left + writeBackOffset + i * baseChunkSize, mergeBlock);
                            mergeBlock = new Rectangle[baseChunkSize];
                            mergeCount = 0;
                            //mergeBlock.Clear();
                        }
                        else
                        {
                            this.SaveListToFileAppend(tempFile, mergeBlock);
                            mergeBlock = new Rectangle[baseChunkSize];
                            mergeCount = 0;
                            //mergeBlock.Clear();
                        }

                    }

                    if (!firstLevel)
                    {
                        SetRectangleByBlock(left + i * chunkSize, tempFile);
                    }
                }

                firstLevel = false;
                noChunks /= 2;
                chunkSize *= 2;
                baseToLoad *= 2;
            }

            _bw.Flush();
            _fs.Flush();
            //PrintTree();
            //Console.WriteLine($"Sorted {left} - {right}");
        }

        private int CalculateNoOfChunks(long left, long right)
        {
            int chunks = 1;
            long size = (right - left + 1) * NODE_SIZE;
            while (size > MAX_MEMORY)
            {
                chunks *= 2;
                size /= 2;
            }

            return chunks;
        }

        // No References
        //public void FastSort(int left, int right, IComparer<Rectangle> comparer)
        //{
        //    Rectangle[] data = new Rectangle[right - left + 1];
        //    for (int i = 0; i <= data.Length - 1; i++)
        //    {
        //        data[i] = GetRectangleByIndex(left + i);
        //    }

        //    Array.Sort(data, comparer);
        //    for (int i = 0; i <= data.Length - 1; i++)
        //    {
        //        this.SetRectangleByIndex(left + i, data[i]);
        //    }

        //}

        public override string ToString()
        {
            StringBuilder output = new StringBuilder();
            int i = 0;
            _fs.Seek(0, SeekOrigin.Begin);
            while ((_fs.Position < _fs.Length))
            {
                float x = _br.ReadSingle();
                float y = _br.ReadSingle();
                Rectangle r = new Rectangle(x, y, x, y);
                output.AppendLine($"{i} : {r.ToString()}");
                i++;
            }

            return output.ToString();
        }
    }
}


